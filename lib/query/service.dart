import 'dart:io';

import 'package:http/http.dart';

abstract class SearchService {
  Future<String> query(String query, {int limit = 200});
}

class HttpSearchService implements SearchService {
  @override
  Future<String> query(String query, {int limit = 200}) async {
    final Uri uri = Uri.http("109.87.29.252:8080", "search", <String, String>{
      "query": query,
      "limit": limit.toString(),
    });
    final Response response = await get(uri);
    if (response.statusCode >= 200 && response.statusCode < 300) {
      return response.body;
    } else {
      throw HttpException(response.body);
    }
  }
}
