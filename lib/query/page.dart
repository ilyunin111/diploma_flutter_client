import 'package:diploma_flutter_client/query/bloc.dart';
import 'package:diploma_flutter_client/query/factory.dart';
import 'package:diploma_flutter_client/query/widget.dart';
import 'package:flutter/material.dart';

class SearchPage extends StatefulWidget {
  @override
  _SearchPageState createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  final TextEditingController _queryController = TextEditingController();
  final TextEditingController _countController = TextEditingController();
  SearchBloc _bloc;

  @override
  void initState() {
    super.initState();
    _bloc = SearchBlocFactory().create();
  }

  @override
  void dispose() {
    _bloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SearchWidget(
      bloc: _bloc,
      queryController: _queryController,
      countController: _countController,
    );
  }
}
