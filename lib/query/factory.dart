import 'package:diploma_flutter_client/query/bloc.dart';
import 'package:diploma_flutter_client/query/facade.dart';
import 'package:diploma_flutter_client/query/service.dart';

class SearchBlocFactory {
  SearchBloc create() {
    return SearchBloc(SearchFacade(HttpSearchService()));
  }
}
