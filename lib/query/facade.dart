import 'package:diploma_flutter_client/query/service.dart';

class SearchFacade {
  final SearchService _service;

  SearchFacade(this._service);

  Future<String> search(String query) => _service.query(query, limit: 200);

}