import 'package:diploma_flutter_client/query/bloc.dart';
import 'package:flutter/material.dart';

class SearchWidget extends StatelessWidget {
  final SearchBloc bloc;
  final TextEditingController queryController;
  final TextEditingController countController;

  const SearchWidget({
    @required this.bloc,
    @required this.queryController,
    @required this.countController,
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Search Client"),
      ),
      body: StreamBuilder<SearchState>(
        stream: bloc.stateStream,
        builder: (_, AsyncSnapshot<SearchState> snapshot) {
          if (snapshot.hasData) {
            return _buildBasedOnState(snapshot.data);
          } else {
            return _buildRequestForm();
          }
        },
      ),
    );
  }

  Widget _buildBasedOnState(SearchState state) {
    if (state.isProcessing) {
      return Center(
        child: CircularProgressIndicator(),
      );
    } else if (state.isSuccessful) {
      return Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text("Success! Requests performed: ${state.queriesPerformed}"),
          ),
          Expanded(
            child: _buildRequestForm(),
          )
        ],
      );
    } else {
      return Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text("Error occurred: ${state.error.toString()}"),
          ),
          Expanded(
            child: _buildRequestForm(),
          )
        ],
      );
    }
  }

  Widget _buildRequestForm() {
    return Column(
      children: <Widget>[
        Spacer(flex: 1),
        Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Text("Query:"),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextField(controller: queryController),
            ),
          ],
        ),
        Spacer(flex: 1),
        Row(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text("Requests:"),
            ),
            Flexible(
              child: TextField(
                controller: countController,
                keyboardType: TextInputType.number,
              ),
            ),
          ],
        ),
        Spacer(flex: 1),
        MaterialButton(
          child: Text("Run!"),
          onPressed: () => bloc.search(
            queryController.text,
            int.parse(countController.text),
          ),
        ),
        Spacer(flex: 1),
      ],
    );
  }
}
