import 'dart:async';

import 'package:diploma_flutter_client/query/facade.dart';
import 'package:rxdart/rxdart.dart';

class SearchBloc {
  final BehaviorSubject<SearchState> _subject = BehaviorSubject<SearchState>();

  final StreamController<SearchEvent> _eventController =
      StreamController<SearchEvent>();

  final SearchFacade _facade;

  SearchBloc(this._facade) {
    _eventController.stream
        .asyncExpand((SearchEvent event) => event._apply(_subject.value))
        .listen(_subject.add);
  }

  void search(String query, int requests) {
    _eventController.add(_PerformSearch(requests, query, _facade));
  }

  Stream<SearchState> get stateStream => _subject;

  void dispose() {
    _subject.close();
    _eventController.close();
  }
}

abstract class SearchEvent {
  Stream<SearchState> _apply(SearchState state);
}

class _PerformSearch implements SearchEvent {
  final int _requestsCount;
  final String _query;
  final SearchFacade _facade;

  _PerformSearch(this._requestsCount, this._query, this._facade);

  @override
  Stream<SearchState> _apply(SearchState state) async* {
    if (!(state?.isProcessing ?? false)) {
      yield SearchState._processing();
      try {
        final int successfulRequests = (await Future.wait(
                List<int>.generate(_requestsCount, (_) => _)
                    .map((_) => _facade.search(_query))))
            .length;
        yield SearchState._success(successfulRequests);
      } on Exception catch (e) {
        print(e.toString());
        yield SearchState._error(e);
      }
    }
  }
}

class SearchState {
  final Object error;
  final int queriesPerformed;

  SearchState._(this.error, this.queriesPerformed)
      : assert(error == null || queriesPerformed == null);

  SearchState._processing() : this._(null, null);

  SearchState._success(int queries) : this._(null, queries);

  SearchState._error(Object error) : this._(error, null);

  bool get isProcessing => error == null && queriesPerformed == null;

  bool get hasError => error != null;

  bool get isSuccessful => queriesPerformed != null;
}
